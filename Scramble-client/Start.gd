extends Button

# Setting fullscreen in browesrs is only allowed in the input function...
func _input(ev):
    if not OS.window_fullscreen:
        OS.window_fullscreen = true
    self.queue_free()