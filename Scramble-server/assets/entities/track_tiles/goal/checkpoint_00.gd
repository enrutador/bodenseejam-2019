extends Area

func _process(delta):
	var affected_players = self.get_overlapping_bodies()
	for affected_player in affected_players:
		affected_player.checkpoint = 1