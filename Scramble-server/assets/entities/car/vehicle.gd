extends VehicleBody

# Member variables
const STEER_SPEED = 2
const STEER_LIMIT = 0.5

var steer_angle = 0
var steer_target = 0

var respawn
var playerName
export var checkpoint = 0
export var labs = 0
export var engine_force_value = 40
export var break_force = 40

func updateName():
    var label = get_node("../Viewport/GUI/CenterContainer/Label")
    label.set_text(playerName + " - " + String(labs))

func set_respawn(point):
    self.respawn = point
    
func respawn():
    self.angular_velocity = Vector3(0, 0, 0)
    self.linear_velocity =Vector3(0, 0, 0)
    self.global_transform = self.respawn

func setCarName(name):
    playerName = name
    var label = get_node("../Viewport/GUI/CenterContainer/Label")
    label.set_text(playerName + " - " + String(labs))

func _ready():
    self.respawn = self.global_transform
    var mat = get_node("BODY").get_surface_material(0)
    mat.albedo_color = Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1))
    get_node("BODY").set_surface_material(0, mat)

func _physics_process(delta):

    var client_id = str(get_parent().name)
    
    var fwd_mps = transform.basis.xform_inv(linear_velocity).x
    
    steer_angle = Global.client_inputs[client_id].steering_angle * STEER_LIMIT
    
    if Global.client_inputs[client_id].forward:
        engine_force = engine_force_value
    else:
        engine_force = 0
    
    if Global.client_inputs[client_id].backward:
        if (fwd_mps >= -1):
            engine_force = -engine_force_value
        else:
            brake = break_force
    else:
        brake = 0.0

    steering = steer_angle * -1
    
    var ui = get_node("../Area")
    var currPos = self.translation;
    ui.translation = Vector3(currPos.x,currPos.y+8,currPos.z)


