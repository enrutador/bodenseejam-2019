extends Node

var timeout = 10
var evil_world = false
onready var traps_node = self.get_node("traps")
onready var replacement_node = self.get_node("replacement")
#signal evil_world_signal
#signal normal_world_signal

# Called when the node enters the scene tree for the first time.
func _ready():
	#traps_node.hide()
	#emit_signal("normal_world_signal")
	self.remove_child(traps_node)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timeout -= delta
	if timeout < 0:
		timeout = 10
		if evil_world:
			self.add_child(replacement_node)
			self.remove_child(traps_node)
			
			#traps_node.hide()
			#emit_signal("normal_world_signal")
			self.get_node("DirectionalLight").set_color(Color("ffffff"))
		else:
			self.add_child(traps_node)
			self.remove_child(replacement_node)
			#traps_node.show()
			#emit_signal("evil_world_signal")
			self.get_node("DirectionalLight").set_color(Color("e000e0"))
		evil_world = !evil_world